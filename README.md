INTRODUCTION
------------

This module integrates the 'Typed.js' library:
- https://mattboldt.com/demos/typed-js/

Typed.js is a library that types. Enter any string, and watch it type at
the speed you've set, backspace what it's typed, and begin a new sentence
for however many strings you've set.


FEATURES
--------

'Typed.js' library is:

  - Cross-browser animations

  - Usage with Javascript

  - Easy to use

  - Responsive

  - Customizable


REQUIREMENTS
------------

'Typed.js' library:

  - https://github.com/mattboldt/typed.js/archive/main.zip


INSTALLATION
------------

1. Download 'Typed' module - https://www.drupal.org/project/typed

2. Extract and place it in the root of contributed modules directory i.e.
   /modules/contrib/typed

3. Create a libraries directory in the root, if not already there i.e.
   /libraries

4. Create a 'typed.js' directory inside it i.e.
   /libraries/typed.js

5. Download 'Typed.js' library
   https://github.com/mattboldt/typed.js/archive/main.zip

6. Place it in the /libraries/typed.js directory i.e. Required files:

  - /libraries/typed.js/src/typed.js
  - /libraries/typed.js/dist/typed.umd.js

7. Now, enable 'Typed.js' module


USAGE
-----

It’s very simple to use a library, This is really all you need to get going:

var typed = new Typed('.element', {
  strings: ["First sentence.", "Second sentence."],
  typeSpeed: 30
});


How does it Work?
-----------------

1. Enable "Typed" module, Follow INSTALLATION in above.

2. Add element class to your theme/module, Follow USAGE in above.

3. Enjoy that.

Typed.js is a library that types.


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt


DEMO
----
https://mattboldt.com/demos/typed-js/
